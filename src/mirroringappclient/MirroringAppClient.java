/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mirroringappclient;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import remote_interfaces.RemoteFileInterface;
import remote_interfaces.RemoteListener;

/**
 *
 * @author Dariusz Kulig
 */
public class MirroringAppClient extends UnicastRemoteObject implements RemoteListener {
    /**
    * Zmienna odwołująca się do rejestru w celu pobrania zdalnego obiektu.
    */
    public Registry myRegistry;
    /**
    * Zmienna będąca referencją do zdalnego obiektu.
    */
    public RemoteFileInterface remoteFileInterface;
    /**
    * Konstruktor klasy MirroringAppClient                         
    * <p>
    * Wewnątrz konstruktora następuje pobranie referencji do zdalnego obiektu
    * w przypadku niepowodzenia aplikacja zakończy działanie
    * </p>
    *
    */
    public MirroringAppClient() throws RemoteException{
        System.out.println("Looking for mirroring server");
        try {
            myRegistry = LocateRegistry.getRegistry("192.168.1.105", 1099);
            remoteFileInterface = (RemoteFileInterface) myRegistry.lookup("remoteFileInterface");
            remoteFileInterface.addRemoteListener(this);
        } catch (NotBoundException nbe) {
            JOptionPane.showMessageDialog(null, "Error while connecting to the server.","Connecting Error",
            JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        } catch (RemoteException re) {
           JOptionPane.showMessageDialog(null, "Error while obtaining RMI stub.","Software Error",
           JOptionPane.ERROR_MESSAGE);
           re.printStackTrace();
           System.exit(0);
        }
    }
    /**
    * Przeciążona metoda z interfejsu RemoteListener                           
    * <p>
    * Metoda ta ma za zadanie wysłanie wiadomości tekstowej do wszystkich klientów
    * podłączonych do serwera zdalnego.
    * </p>
    * @param  message zawiera zawartość wiadomości.         
    * @return Zwraca wiadomość tekstową wysłaną do wszystkich klientów.
    */
    @Override
    public String sendMessageToClients(String message) throws java.rmi.RemoteException {
        System.out.println(message);
        return message;
    }
    
    /**
    * Przeciążona metoda z interfejsu RemoteListener.   .                       
    * <p>
    * Metoda ta ma za zadanie pobrać plik ze zdalnego serwera.
    * </p>
    * @param  fileName zawiera nazwę pliku do pobrania.         
    * @return Zwraca tablicę bajtów z których plik się składa.
    */
    @Override
    public byte[] downloadFile(String fileName) throws RemoteException {
        try{
            File file = new File(fileName);
            byte [] buffer = remoteFileInterface.downloadFile(fileName);
            BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file.getName()));
            output.write(buffer,0,buffer.length);
            output.flush();
            output.close();
        }catch(IOException e){
            JOptionPane.showMessageDialog(null, "There was an error while downloading file.","Download Error",
            JOptionPane.ERROR_MESSAGE);
        }
        return remoteFileInterface.downloadFile(fileName);
    }
    /**
    * Przeciążona metoda z interfejsu RemoteListener.                          
    * <p>
    * Metoda ta ma za zadanie załadować plik na serwer. Upload pliku odbywa się w
    * osobnym wątku aby nie blokować główengo wątku.
    * @param  fileName zawiera nazwę pliku do załadowania.
    * @param  buffer zawiera tablicę bajtów z których się plik składa. Jest ona wysyłana na serwer.
    */
    @Override
    public void uploadFile(String fileName, byte[] buffer) throws RemoteException {
        Thread t = new Thread(new Runnable(){
            public void run(){
                try {
                    remoteFileInterface.uploadFile(fileName,buffer);
                } catch (RemoteException ex) {
                    Logger.getLogger(MirroringAppClient.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        t.start();
    }
}
