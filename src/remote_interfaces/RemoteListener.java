/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remote_interfaces;

/**
 *
 * @author darglk
 */
public interface RemoteListener extends java.rmi.Remote{
    public String sendMessageToClients(String message) throws java.rmi.RemoteException;
    public byte[] downloadFile(String fileName) throws java.rmi.RemoteException;
    public void uploadFile(String fileName, byte[] buffer) throws java.rmi.RemoteException;
}
